﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(bodev.Startup))]
namespace bodev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
